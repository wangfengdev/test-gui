import logo from './logo.svg';
import './App.css';
import { createElement, useState, useEffect, useRef } from 'react';


console.log("in App domain.");





function App() {
	console.log("App()");



	const onRun = function () {
		console.log("on run.");
		const scriptTag = document.createElement("script");
		scriptTag.src = "test.js";
		scriptTag.async = true;
		document.body.appendChild(scriptTag);
	}

	return (
		<div className="App">
			<button onClick={onRun}>run script</button>
			<div>Container</div>
			<div>
				This App Container.
			</div>

		</div>
	);
}
console.log("end App domain.");
export default App;


